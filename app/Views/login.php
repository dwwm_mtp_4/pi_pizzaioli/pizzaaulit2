<?php
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/login.css">
    <title>Pizza_au_lit</title>
</head>
<body>
    <h1>Benvenuto a Pizza'au'lit</h1>
    <div class="container right-panel-active">
        <!-- Sign Up -->
        <div class="container__form container--signup">
            <form action="#" class="form" id="form1">
                <h2 class="form__title">S'inscrire</h2>
                <input type="text" placeholder="Pseudo" class="input" />
                <input type="email" placeholder="Email" class="input" />
                <input type="password" placeholder="Mot De Passe" class="input" />
                <input type="password2" placeholder="Verifier Mot De Passe" class="input" />
                <button class="btn">S'inscrire</button>
            </form>
        </div>
    
        <!-- Sign In -->
        <div class="container__form container--signin">
            <form action="#" class="form" id="form2">
                <h2 class="form__title">Se connecter</h2>
                <input type="email" placeholder="Email" class="input" />
                <input type="password" placeholder="Mot De Passe" class="input" />
                <a href="#" class="link">Mot de passe oublié?</a>
                <button class="btn">Se connecter</button>
            </form>
        </div>
    
        <!-- Overlay -->
        <div class="container__overlay">
            <div class="overlay">
                <div class="overlay__panel overlay--left">
                    <button class="btn" id="signIn">Se connecter</button>
                </div>
                <div class="overlay__panel overlay--right">
                    <button class="btn" id="signUp">S'inscrire</button>
                </div>
            </div>
        </div>
    </div>
<script src="../js/login.js"></script>
</body>
</html>